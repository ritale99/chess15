package PieceTypes;
/**
 * The EmptySquare class is a class that contains no Piece.
 * @author Rithvik Aleshetty
 * @author Harsh Patel
 *
 */

public class EmptySquare extends Piece {

	public EmptySquare(String value) {
		super(value);
	}

}
